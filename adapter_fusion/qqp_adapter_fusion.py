import torch
import os
from datasets import load_dataset
from transformers import BertTokenizer, BertConfig, BertAdapterModel, TrainingArguments, AdapterTrainer, EvalPrediction, AdapterConfig
from transformers.adapters.composition import Fuse
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score

os.environ['CUDA_VISIBLE_DEVICES']="0,1"
if torch.cuda.is_available():
  print('CUDA available')
else:
  print('CUDA not available')


dataset_train, dataset_valid = load_dataset("glue", "qqp", split=['train', 'validation'])
dataset_train_test = dataset_train.train_test_split(test_size=0.15, shuffle=True, seed=23, stratify_by_column="label")
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

def encode_batch(batch):
  """Encodes a batch of input data using the model tokenizer."""
  return tokenizer(
      batch["question1"], 
      batch["question2"], 
      truncation="longest_first", 
      padding="max_length",
      max_length=350,
  )

# Encode the input data
dataset_train_test = dataset_train_test.map(encode_batch, batched=True)
dataset_valid = dataset_valid.map(encode_batch, batched=True)

dataset_train_test = dataset_train_test.rename_column("label", "labels")
dataset_valid = dataset_valid.rename_column("label", "labels")

# Transform to pytorch tensors and only output the required columns
dataset_train_test.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])
dataset_valid.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])


id2label = {id: label for (id, label) in enumerate(dataset_train_test["train"].features["labels"].names)}

config = BertConfig.from_pretrained(
    "bert-base-uncased",
    id2label=id2label,
)
model = BertAdapterModel.from_pretrained(
    "bert-base-uncased",
    config=config,
)

# Load the pre-trained adapters we want to fuse
model.load_adapter("sts/mrpc@ukp", config=AdapterConfig.load("pfeiffer"), with_head=False, load_as="mrpc")
model.load_adapter("sts/stackexchange@ukp", config=AdapterConfig.load("pfeiffer", reduction_factor=12), with_head=False, load_as="stackexchange")
model.load_adapter("sts/sts-b@ukp", config=AdapterConfig.load("pfeiffer"), with_head=False, load_as="sts-b")
model.load_adapter("WillHeld/pfadapter-bert-base-uncased-tada-value-eraser", source="hf", with_head=False, load_as="glue-nli")
# Add a fusion layer for all loaded adapters
model.add_adapter_fusion(Fuse("mrpc", "stackexchange", "sts-b", "glue-nli"))
model.set_active_adapters(Fuse("mrpc", "stackexchange", "sts-b", "glue-nli"))

# Add a classification head for our target task
model.add_classification_head("classification_head", num_labels=len(id2label))

# Unfreeze and activate fusion setup
adapter_setup = Fuse("mrpc", "stackexchange", "sts-b", "glue-nli")
model.train_adapter_fusion(adapter_setup)

training_args = TrainingArguments(
    learning_rate=1e-4,
    num_train_epochs=5,
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    logging_steps=5000,
    evaluation_strategy="steps",
    do_predict=True,
    save_strategy="steps",
    eval_steps=5000,
    save_steps=5000,
    load_best_model_at_end=True,
    warmup_ratio=0.1,
    logging_first_step=True,
    output_dir="./qqp_training_output",
    overwrite_output_dir=True,
    save_total_limit=5,
    resume_from_checkpoint='training_output/checkpoint-25000',
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
)

def compute_accuracy(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  return {
      "acc": accuracy_score(p.label_ids, preds),
      "f1": f1_score(p.label_ids, preds),
      "precision": precision_score(p.label_ids, preds),
      "recall": recall_score(p.label_ids, preds)
      }

trainer = AdapterTrainer(
    model=model,
    args=training_args,
    train_dataset=dataset_train_test["train"],
    eval_dataset=dataset_valid,
    compute_metrics=compute_accuracy,
)

trainer.train()

trainer.evaluate()

print(trainer.predict(dataset_train_test["test"]))

model.save_adapter_fusion("./qqp_adapter_fusion_fusion", "mrpc,stackexchange,sts-b,glue-nli")
model.save_all_adapters("./qqp_adapter_fusion_adapters")

def predict(text1, text2):
  encoded = tokenizer(text1, text2, return_tensors="pt")
  if torch.cuda.is_available():
    encoded.to("cuda")
  logits = model(**encoded)[0]
  pred_class = torch.argmax(logits).item()
  return id2label[pred_class]

predict(
    'The procedure is generally performed in the second or third trimester .', 
    'The technique is used during the second and , occasionally , third trimester of pregnancy .'
)