% Paper template for TAR 2022
% (C) 2014 Jan Šnajder, Goran Glavaš, Domagoj Alagić, Mladen Karan
% TakeLab, FER

\documentclass[10pt, a4paper]{article}

\usepackage{tar2022}

\usepackage[utf8]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow}

\title{Comparing Different Multi-Task Learning Methods on Paraphrase Identification Tasks}

\name{Tin Ferković} 

\address{
University of Zagreb, Faculty of Electrical Engineering and Computing\\
Unska 3, 10000 Zagreb, Croatia\\ 
\texttt{tin.ferkovic@fer.hr}\\
}
          
         
\abstract{ 
In multi-task learning (MTL), the model is trained to perform multiple tasks simultaneously. An experiment with three different modular MTL approaches is conducted. The results are compared using MRPC and QQP datasets. Adapter fusion proved effective on a low-resource dataset, while adapter tuning achieved better metrics using a high-resource dataset. Conditionally-Adaptive MTL (CA-MTL) underperformed in the experiments, and the reasons for this underperformance are discussed.
}

\begin{document}

\maketitleabstract

\section{Introduction}

Given $N$ tasks $\{T_i\}_{i=1}^N$ with $N$ different datasets $\{D_i\}_{i=1}^N$, multi-task learning (MTL) leverages information from all or a subset of them in order to improve all the tasks' performance. It was first introduced by \citet{caruana1997multitask}. An intuition behind it is that, for example, a human can learn multiple tasks simultaneously, and two related tasks could benefit each other, such as how learning how to skate could help in learning how to ski. This is different from transfer learning, multi-label learning/multi-output regression, and multi-view learning \citep{zhang2021survey}.

MTL is a technique that allows for a single model to be trained on multiple tasks, resulting in shared and task-specific parameters. Benefits of MTL include: a single model with fewer parameters, shared knowledge between tasks, better generalization through representation bias \citep{zhou2019overview}, increased data efficiency, leveraging auxiliary information and eavesdropping. However, there are challenges, which include catastrophic forgetting \citep{pmlr-v80-serra18a} and interference \citep{pilault2020conditionally}, as well as a need for datasets sampling and task balancing \citep{crawshaw2020multi}.

The essence of MTL is sharing the information between the tasks by sharing their parameters. There are two sharing mechanisms -- hard and soft parameter sharing \citep{ruder2017overview}. \emph{Hard parameter sharing} uses a shared representation on top of which the task-specific parameters are placed (usually referred to as heads). \emph{Soft parameter sharing} uses different parameters for each task, but the distances between these are encouraged to be smaller by using the loss function. Hard parameter sharing is more common due to its parameter sharing mechanism which reduces both the risk of overfitting to specific tasks, as well as the number of model's parameters.

Adapter residual modules were first introduced in \citet{rebuffi2017learning} for the visual domain.
However, in \citet{houlsby2019parameter} they were first used for the NLP domain. 
Adapters are small, task-specific modules which are inserted within network layers. In this work, this network is BERT \citep{devlin2018bert}. Compared to Transformers' sizes \citep{vaswani2017attention}, they add only a few parameters per task. The parameters of the original network remain frozen, which results in a high degree of parameter sharing. Due to that, adapters for new tasks can be easily added without retraining the Transformer or other adapters.

A hypernetwork is a network that generates the weights of another network, also known as a main network \citep{ha2016hypernetworks}. The main network is typically a neural network that is used for a specific task, such as text classification or sequence labelling. The hypernetwork, on the other hand, is trained to generate the weights for the main network, which can then be used to perform the desired task. This approach allows for more efficient use of resources and can lead to improved performance on certain tasks. Additionally, hypernetworks have been shown to be useful in multi-task learning scenarios where a single network is trained to perform multiple tasks. In these scenarios, the hypernetwork can generate task-specific weights for the main network, allowing it to adapt to each task and potentially improve overall performance \citep{mahabadi2021parameter}.

In this work, two adapter approaches are used -- adapter tuning \citep{houlsby2019parameter} and adapter fusion \citep{pfeiffer2020adapterfusion}. These approaches are compared with Conditionally-Adaptive MTL (CA-MTL). CA-MTL uses a hypernetwork to generate or modify the parameters, making them task-specific. The approaches are investigated using two GLUE paraphrase identification datasets of very different sizes -- Microsoft Research Paraphrase Corpus (MRPC) and Quora Question Pairs (QQP) \citep{wang2018glue}.

\section{Related Work and Methods}

\subsection{Adapter Tuning}

In the original work \cite{houlsby2019parameter}, adapter is added twice within a Transformer layer -- once after attention sub-layer, and once after feed-forward sub-layer. Both times adapter is added after projecting the output back into input space and before adding the skip connection. In my, as well as some previous experiments \citep{pfeiffer2020madx}, adapter is added only after the feed-forward layer. Alongside the layers in adapter module, new layer normalization parameters are trained per task. The original architecture is shown in Figure~\ref{fig:houlsby}.

\begin{figure}[]
\centering
\includegraphics[width=\columnwidth]{figures/houlsby.png}
\caption{Adapter architecture from \cite{houlsby2019parameter}. \textbf{Left:} They add the adapter module twice to each Transformer layer. In my experiments, it is added only after the feed-forward layer. \textbf{Right:} The adapter consists of a bottleneck which contains few parameters relative to the attention and feed-forward layers in the original model. The adapter also contains a skip-connection. During adapter tuning, the green layers are trained on the downstream data, this includes the adapter, the layer normalization parameters, and the final classification layer (not shown in the figure).}
\label{fig:houlsby}
\end{figure}

\citet{houlsby2019parameter} indicates a few tricks to adapter training. To start with, near identity initialization proved to be crucial for a stable adapter training. That means that adapters' parameters should be initialized such that they have no effect on the Transformer's representation of the input text. Moreover, adapters in lower layers have less impact (pruning them degrades performance less than pruning adapters from higher layers), presumably because they extract lower-level features that are shared among tasks, while higher layers build features unique to specific tasks. Finally, adapter sizes (8, 16, 256, etc.) yield only a small difference in performance, allowing the use of the same adapter sizes for all the tasks.

A drawback of this approach is the fact that most of the MTL benefits are absent -- representation bias, data efficiency, leveraging auxiliary information, and eavesdropping. However, none of the MTL challenges are present either. According to \citet{semnani2019bert}, training adapters' and BERT's weights simultaneously performed poorly. Instead, an approach of pre-training BERT, freezing all its parameters, inserting adapter modules, then training adapter weights on the available dataset for some epochs is taken.


\subsection{Adapter Fusion}

Adapter fusion has two stages -- \textit{knowledge extraction} and \textit{knowledge composition}. In the first phase, task-specific adapters are trained separately, as before. In the second phase, these adapters are combined together. It uses multiple adapters to maximize knowledge transfer between tasks without suffering from the MTL drawbacks, such as catastrophic forgetting or interference. With simple adapters \cite{houlsby2019parameter}, their distinct weights prevent a downstream task from being able to use multiple sources of extended information, i.e., there is no task sharing. Adapter fusion introduces a new set of weights which learn to combine the adapters as a dynamic function of the target task data, as presented in Figure~\ref{fig:adapterfusion}. This is a major downside of adapter fusion -- it needs to be trained for a single target task only.

\begin{figure}[]
\centering
\includegraphics[width=3.5cm]{figures/adapterfusion.png}
\caption{Adapter fusion architecture from \citet{pfeiffer2020adapterfusion}. The adapter fusion component takes as input the representations of multiple adapters trained on different tasks and learns a parameterized mixer of the encoded information.}
\label{fig:adapterfusion}
\end{figure}

Adapter fusion learns to identify the most useful adapter for the given input. It uses attention in doing so. At each layer \emph{l} and time-step \emph{t}, \emph{query} takes as input the output of the pre-trained Transformer's feed-forward sub-layer of layer \emph{l}, while both \emph{key} and \emph{value} take as input the output of the respective adapters. Query, key, and value matrices are different at each layer \emph{l}.


\subsection{CA-MTL}

CA-MTL \citep{pilault2020conditionally} tackles the problem of modularizing a pre-trained network with latent task representations. They either add conditional layers or modulate existing pretrained weights using a task representation. Their task-conditioned Transformer has 4 components: (1) conditional attention, (2) conditional alignment, (3) conditional layer normalization, and (4) conditional bottleneck. Conditional attention adds a block diagonal conditional matrix to the original attention equation. Task-conditional alignment is added between the input embedding layer and first Transformer layer. Conditional layer normalization is added to account for task-specific rescaling of individual training cases. Finally, a conditional bottleneck (adapter) is added to the topmost Transformer layer(s). In their public implementation, and consequently in the experiments presented here, adapter is added only to the single topmost Transformer layer. \footnote{https://github.com/CAMTL/CA-MTL} In summary, as they state, \textit{CA-MTL is a hypernetwork adapter}.

Multi-task uncertainty sampling is used. The algorithm first evaluates model's uncertainty. It uses Shannon Entropy, an uncertainty measure, to choose training samples by first doing forward pass through the model with $b \times T$ input samples. In doing so, it favours tasks with the highest uncertainty. Tasks are sampled whenever model sees entropy increase, helping to avoid catastrophic forgetting.

\section{Experiments}
\label{sec:experiments}

\subsection{Datasets}

MRPC is a binary classification dataset where each instance contains two sentences and a label 0 or 1. Label 0 indicates no paraphrase, while label 1 indicates that the sentences paraphrase each other. It is used as is from the GLUE benchmark \citep{wang2018glue}, with 3,668 training, 408 validation, and 1,725 test instances.

QQP is also a binary classification dataset where each instance contains two Quora questions and a label 0 or 1. \footnote{https://www.quora.com/} Label 0 indicates that the questions are referring to different things, while label 1 indicates they are referring to the same thing. Unlike MRPC, this dataset needed a new test split, because the default one has no labels. For that reason, training set was split into 309,269 training and 54,577 testing instances. Validation set was used as is, with 40,430 instances.

For adapter tuning and adapter fusion experiments, a base, uncased version of a BERT tokenizer was used. Sequences were padded to the maximum length of 350, truncating the longer one first in case of longer sequences. For the CA-MTL experiment, a base, cased version of BERT tokenizer was used. Here, due to a larger memory usage, the sequences were padded to a maximum length of 192, again truncating the longer one first. These differences need to be kept in mind when comparing the models.

\subsection{Setup}

For adapter fusion, there was a choice of adapters one wants to fuse. For the MRPC experiment, adapters used were QQP \citep{chen2018quora}, StackExchange \citep{rueckle-etal-2020-multicqa}, STS-B \citep{cer2017semeval}, and GLUE-NLI \citep{wang2018glue}. For the QQP experiment, MRPC adapter \citep{Dolan2005AutomaticallyCA} substituted the QQP adapter. All the adapters were taken from AdapterHub \citep{pfeiffer2020adapterhub}. \footnote{https://adapterhub.ml/} 

All the adapter tuning and adapter fusion experiments were run with the learning rate of $1 \times 10^{-4}$ and a warmup ratio of 0.1. CA-MTL experiment had no warmup and used the learning rate of $5 \times 10^{-5}$. The best model for the experiments was loaded from a checkpoint with the lowest validation set loss. Additionally, in the case of CA-MTL, a model with the highest validation F1-score was also loaded for comparison. Adapter training experiments had the batch size of 16, adapter fusion of 8, and CA-MTL of 16 with the gradient accumulation of 2 (effective batch size of 32). For both MRPC experiments (adapter tuning and adapter fusion), the number of epochs was 15. For QQP, it was 5 due to a larger dataset. CA-MTL experiment was run for 2 epochs only, since its training was slower due to the MT uncertainty sampling. All the experiments used Adam with decoupled weight decay (AdamW) optimizer \citep{loshchilov2017adamw}, a linearly decreasing scheduler, and were performed on either a single (adapter tuning and adapter fusion) or two (CA-MTL) NVIDIA GeForce GTX 1080 GPUs with 8~GiB of memory.

\section{Results and Discussion}

Results of all the experiments are presented in Table~\ref{table:results}. 
\begin{table*}[ht]
\centering
\begin{tabular}{c c c c c c}
\hline
\multicolumn{1}{c}{\textbf{Task}} & \multicolumn{1}{c}{\textbf{Model}} & \multicolumn{1}{c}{\textbf{Accuracy}} & \multicolumn{1}{c}{\textbf{F1}} & \multicolumn{1}{c}{\textbf{Precision}} & \multicolumn{1}{c}{\textbf{Recall}}\\
\hline
\multirow{4}{*}{MRPC} & Adapter tuning & 0.79 & 0.85 & 0.81 & 0.89\\
& Adapter fusion & \textbf{0.83} & \textbf{0.88} & \textbf{0.85} & 0.90\\
& CA-MTL (min. loss) & 0.77 & 0.84 & 0.78 & 0.92\\
& CA-MTL (max. F1) & \textbf{0.83} & \textbf{0.88} & 0.84 & \textbf{0.93}\\
\hline
\multirow{4}{*}{QQP} & Adapter tuning & \textbf{0.89} & \textbf{0.85} & \textbf{0.83} & 0.87\\
& Adapter fusion & 0.83 & 0.79 & 0.75 & 0.83\\
& CA-MTL (min. loss) & 0.81 & 0.78 & 0.67 & \textbf{0.93}\\
& CA-MTL (max. F1) & 0.86 & 0.83 & 0.75 & 0.91\\
\hline
\end{tabular}
\caption{Performance comparison of different models on MRPC and QQP tasks. Best model's metrics for each dataset are in \textbf{bold}. CA-MTL (min. loss) indicates test results of a model selected using the minimal validation loss criteria, while CA-MTL (max. F1) used a maximal validation F1-score criteria.}
\label{table:results}
\end{table*}
Adapter tuning and adapter fusion were run once on MRPC and QQP datasets. CA-MTL was run once on both datasets simultaneously using MT uncertainty sampling. Due to many more training examples, adapter tuning performs better than adapter fusion on QQP dataset, unlike the MRPC dataset. This is due to the fact that adapter fusion just learns the fusion of previously learned adapters on different datasets. These different datasets would ideally be similar to the one being trained. When training on a low-resource dataset, such as MRPC, it is ideal as a transfer of information from other, higher-resource datasets occurs. However, when training on a high-resource dataset, such as QQP, fusion parameters are not of a large enough capacity and better results are achieved when training adapters that are injected in each Transformer layer.

A significant performance gap is visible between a CA-MTL model selected using the minimal validation loss criteria, and the one selected using the maximal F1-score criteria (see Table~\ref{table:results}). The former is achieved early during the training, while the latter much later in the traning process (see Figure~\ref{fig:training}). Selecting a model using the latter criteria is okay if one is interested in performance only, with no interpretation. However, in machine learning, one usually does not want the model to be very certain in its incorrect predictions, which is the case when using the latter criteria. For that reason, although depending on the use-case, the former criteria should be used.

The behaviour or training/validation losses and validation F1-scores during the training of CA-MTL is shown in Figure~\ref{fig:training}. 
\begin{figure*}
\begin{center}
\includegraphics[width=\textwidth]{figures/training.png}
\caption{CA-MTL training behaviour. For the first 3,200 steps, logging and evaluation are performed every 200 steps. For the first epoch (except the first 3,200 steps), logging and evaluation are performed every 2,000 steps. For the second epoch, logging and evaluation are done every 10,000 steps.}
\label{fig:training}
\end{center}
\end{figure*}
A most interesting observation is that MRPC validation loss starts increasing very early during the training process, unlike QQP validation loss which has not yet started its ascent. This is due to the difference in the sizes of their datasets, 3,668 compared to 309,269. MT uncertainty sampling, which utilizes Shannon entropy, seems to be unable to mitigate the negative effects of the size differences of available datasets. A possible explanation for such a behaviour is that Shannon entropy does not take into account the actual labels. Instead, it focuses on logits associated with each of the two labels. Train loss behaves as expected. Validation F1-scores plateau relatively early and stay at high levels throughout training.

Although the MRPC and QQP are very similar, a hypernetwork in CA-MTL did not leverage enough auxiliary information, and the results are worse than the adapter tuning and adapter fusion counterparts. Another possible explanation is that having a conditional block diagonal attention in every layer and adding an adapter only to the topmost layer is not enough compared to having an adapter in every layer. As mentioned in Section~\ref{sec:experiments}, one needs to keep in mind the differences in batch sizes, use of warmup ratio, maximal sequence length, and BERT tokenizers (cased and uncased) when making any conclusions. 

Once trained, adapter tuning can predict 100 examples per second, adapter fusion 50, and CA-MTL 130. Adapter fusion is very inefficient, since a single example is passed through all the adapters available before going through the fusion of them. A reason for a shorter inference time using CA-MTL probably lies in the fact that the adapter is added only to the single topmost Transformer layer, while in adapter tuning it is added to all 12 of them. A pass through a conditional alignment, conditional block diagonal attention (all layers), and conditional layer normalization (top half of the layers) thus requires less time than passing through an adapter in every layer. 

\section{Conclusions and Future Work}

Conducted experiments show that the adapter fusion is beneficial on low-resource datasets, while adapter tuning works better on high-resource ones. CA-MTL is a promising approach, but underperforms compared to the other two approaches. Additionally, from this experiment, a few concerns related to the work of \citet{pilault2020conditionally} arise. To start with, conditional adapter is added only to the topmost layer. Then, MT uncertainty sampling does not take into account the actual (true) label of the instance, but instead samples the instances with the highest entropy among the candidates. This does not allow instances for which the model in incorrectly certain to get trained again. Furthermore, the code they made publicly available doesn't seem to utilize the hypernetworks in generating the conditionally-adaptive parameters. This is worrying, since their paper relies on the use of hypernetworks. 

For future work, the experiments could be conducted in identical environments. CA-MTL could also be trained for more epochs. MT uncertainty could get modified or replaced with an approach which takes into account the actual label of training instances. Moreover, adapters could and probably should be added to more than single topmost layer. Finally, the code could be modified so that the task embeddings are utilized and hypernetworks generate the networks of task-conditioned adapters, as well as other conditionally adaptive modules.

%\begin{table}
%\caption{This is the caption of the table. Table captions should be placed \textit{above} the table.}
%\label{tab:narrow-table}
%\begin{center}
%\begin{tabular}{ll}
%\toprule
%Heading1 & Heading2 \\
%\midrule
%One & First row text \\
%Two   & Second row text \\
%Three   & Third row text \\
%      & Fourth row text \\
%\bottomrule
%\end{tabular}
%\end{center}
%\end{table}

%\begin{table*}
%\caption{Wide-table caption}
%\label{tab:wide-table}
%\begin{center}
%\begin{tabular}{llr}
%\toprule
%Heading1 & Heading2 & Heading3\\
%\midrule
%A & A very long text, longer that the width of a single column & $128$\\
%B & A very long text, longer that the width of a single column & $3123$\\
%C & A very long text, longer that the width of a single column & $-32$\\
%\bottomrule
%\end{tabular}
%\end{center}
%\end{table*}

\bibliographystyle{tar2022}
\bibliography{tar2022} 

\end{document}

