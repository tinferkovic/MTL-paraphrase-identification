import torch
import os
from datasets import load_dataset
from transformers import BertTokenizer, BertConfig, BertAdapterModel, TrainingArguments, AdapterTrainer, EvalPrediction, AdapterConfig
from transformers.adapters.composition import Fuse
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score

os.environ['CUDA_VISIBLE_DEVICES']="0,1"
if torch.cuda.is_available():
  print('CUDA available')
else:
  print('CUDA not available')


dataset = load_dataset("glue", "mrpc")
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

def encode_batch(batch):
  """Encodes a batch of input data using the model tokenizer."""
  return tokenizer(
      batch["sentence1"], 
      batch["sentence2"], 
      truncation="longest_first", 
      padding="max_length",
      max_length=350,
  )

# Encode the input data
dataset = dataset.map(encode_batch, batched=True)
# The transformers model expects the target class column to be named "labels"
dataset = dataset.rename_column("label", "labels")
# Transform to pytorch tensors and only output the required columns
dataset.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])


id2label = {id: label for (id, label) in enumerate(dataset["train"].features["labels"].names)}

config = BertConfig.from_pretrained(
    "bert-base-uncased",
    id2label=id2label,
)
model = BertAdapterModel.from_pretrained(
    "bert-base-uncased",
    config=config,
)

# Load the pre-trained adapters we want to fuse
model.load_adapter("sts/qqp@ukp", config=AdapterConfig.load("pfeiffer"), with_head=False, load_as="qqp")
model.load_adapter("sts/stackexchange@ukp", config=AdapterConfig.load("pfeiffer", reduction_factor=12), with_head=False, load_as="stackexchange")
model.load_adapter("sts/sts-b@ukp", config=AdapterConfig.load("pfeiffer"), with_head=False, load_as="sts-b")
model.load_adapter("WillHeld/pfadapter-bert-base-uncased-tada-value-eraser", source="hf", with_head=False, load_as="glue-nli")
# Add a fusion layer for all loaded adapters
model.add_adapter_fusion(Fuse("qqp", "stackexchange", "sts-b", "glue-nli"))
model.set_active_adapters(Fuse("qqp", "stackexchange", "sts-b", "glue-nli"))

# Add a classification head for our target task
model.add_classification_head("classification_head", num_labels=len(id2label))

# Unfreeze and activate fusion setup
adapter_setup = Fuse("qqp", "stackexchange", "sts-b", "glue-nli")
model.train_adapter_fusion(adapter_setup)

training_args = TrainingArguments(
    learning_rate=1e-4,
    num_train_epochs=15,
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    logging_steps=340,
    evaluation_strategy="steps",
    do_predict=True,
    save_strategy="steps",
    eval_steps=340,
    save_steps=340,
    load_best_model_at_end=True,
    warmup_ratio=0.1,
    logging_first_step=True,
    output_dir="./mrpc_training_output",
    overwrite_output_dir=True,
    save_total_limit=4,
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
)

def compute_accuracy(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  return {
      "acc": accuracy_score(p.label_ids, preds),
      "f1": f1_score(p.label_ids, preds),
      "precision": precision_score(p.label_ids, preds),
      "recall": recall_score(p.label_ids, preds)
      }

trainer = AdapterTrainer(
    model=model,
    args=training_args,
    train_dataset=dataset["train"],
    eval_dataset=dataset["validation"],
    compute_metrics=compute_accuracy,
)

trainer.train()

trainer.evaluate()

print(trainer.predict(dataset["test"]))

model.save_adapter_fusion("./mrpc_adapter_fusion_fusion", "qqp,stackexchange,sts-b,glue-nli")
model.save_all_adapters("./mrpc_adapter_fusion_adapters")

def predict(text1, text2):
  encoded = tokenizer(text1, text2, return_tensors="pt")
  if torch.cuda.is_available():
    encoded.to("cuda")
  logits = model(**encoded)[0]
  pred_class = torch.argmax(logits).item()
  return id2label[pred_class]

predict(
    'The procedure is generally performed in the second or third trimester .', 
    'The technique is used during the second and , occasionally , third trimester of pregnancy .'
)