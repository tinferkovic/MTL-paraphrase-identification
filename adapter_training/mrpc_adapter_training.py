# %% [markdown]
# # 1️⃣ Training an Adapter for a Transformer model
# 
# In this notebook, we train an adapter for a **RoBERTa** ([Liu et al., 2019](https://arxiv.org/pdf/1907.11692.pdf)) model for sequence classification on a **sentiment analysis** task using [adapter-transformers](https://github.com/Adapter-Hub/adapter-transformers), the _AdapterHub_ adaptation of HuggingFace's _transformers_ library.
# 
# If you're unfamiliar with the theoretical parts of adapters or the AdapterHub framework, check out our [introductory blog post](https://adapterhub.ml/blog/2020/11/adapting-transformers-with-adapterhub/) first.
# 
# We train a **Task Adapter** for a pre-trained model here. Most of the code is identical to a full finetuning setup using HuggingFace's transformers. For comparison, have a look at the [same guide using full finetuning](https://colab.research.google.com/drive/1brXJg5Mokm8h3shxqPRnoIsRwHQoncus?usp=sharing).
# 
# For training, we use the [movie review dataset by Pang and Lee (2005)](http://www.cs.cornell.edu/people/pabo/movie-review-data/). It contains movie reviews  from Rotten Tomatoes which are either classified as positive or negative. We download the dataset via HuggingFace's [datasets](https://github.com/huggingface/datasets) library.

# %% [markdown]
# ## Installation
# 
# First, let's install the required libraries:

# %% [markdown]
# ## Dataset Preprocessing
# 
# Before we start to train our adapter, we first prepare the training data. Our training dataset can be loaded via HuggingFace `datasets` using one line of code:

# %%
import torch
import os
os.environ['CUDA_VISIBLE_DEVICES']="0,1"
if torch.cuda.is_available():
  print('CUDA available')
else:
  print('CUDA not available')

# %%
from datasets import load_dataset

dataset = load_dataset("glue", "mrpc")

# %% [markdown]
# Every dataset sample has an input text and a binary label:

# %% [markdown]
# Now, we need to encode all dataset samples to valid inputs for our Transformer model. Since we want to train on `roberta-base`, we load the corresponding `RobertaTokenizer`. Using `dataset.map()`, we can pass the full dataset through the tokenizer in batches:

# %%
from transformers import BertTokenizer

tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

def encode_batch(batch):
  """Encodes a batch of input data using the model tokenizer."""
  return tokenizer(
      batch["sentence1"], 
      batch["sentence2"], 
      truncation="longest_first", 
      padding="max_length",
      max_length=350,
  )

# Encode the input data
dataset = dataset.map(encode_batch, batched=True)
# The transformers model expects the target class column to be named "labels"
dataset = dataset.rename_column("label", "labels")
# Transform to pytorch tensors and only output the required columns
dataset.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])

# %% [markdown]
# Now we're ready to train our model...

# %% [markdown]
# ## Training
# 
# We use a pre-trained RoBERTa model from HuggingFace. We use `RobertaModelWithHeads`, a class unique to `adapter-transformers`, which allows us to add and configure prediction heads in a flexibler way.

# %%
from transformers import BertConfig, BertAdapterModel, PfeifferConfig

id2label = {id: label for (id, label) in enumerate(dataset["train"].features["labels"].names)}

config = BertConfig.from_pretrained(
    "bert-base-uncased",
    id2label=id2label,
)
model = BertAdapterModel.from_pretrained(
    "bert-base-uncased",
    config=config,
)

# %% [markdown]
# **Here comes the important part!**
# 
# We add a new adapter to our model by calling `add_adapter()`. We pass a name (`"rotten_tomatoes"`) and [the type of adapter](https://docs.adapterhub.ml/adapters.html#adapter-types) (task adapter). Next, we add a binary classification head. It's convenient to give the prediction head the same name as the adapter. This allows us to activate both together in the next step. The `train_adapter()` method does two things:
# 
# 1. It freezes all weights of the pre-trained model so only the adapter weights are updated during training.
# 2. It activates the adapter and the prediction head such that both are used in every forward pass.

# %%
# Add a new adapter
model.add_adapter("mrpc", config=PfeifferConfig())
# Add a matching classification head
model.add_classification_head(
    "mrpc",
    num_labels=len(id2label)
  )
# Activate the adapter
model.train_adapter("mrpc")

# %% [markdown]
# For training, we make use of the `Trainer` class built-in into `transformers`. We configure the training process using a `TrainingArguments` object and define a method that will calculate the evaluation accuracy in the end. We pass both, together with the training and validation split of our dataset, to the trainer instance.
# 
# **Note the differences in hyperparameters compared to full finetuning.** Adapter training usually required a few more training epochs than full finetuning.

# %%
import numpy as np
from transformers import TrainingArguments, AdapterTrainer, EvalPrediction
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score

training_args = TrainingArguments(
    learning_rate=1e-4,
    num_train_epochs=15,
    per_device_train_batch_size=16,
    per_device_eval_batch_size=16,
    logging_steps=150,
    evaluation_strategy="steps",
    do_predict=True,
    save_strategy="steps",
    eval_steps=150,
    save_steps=150,
    load_best_model_at_end=True,
    warmup_ratio=0.1,
    logging_first_step=True,
    output_dir="./mrpc_training_output",
    overwrite_output_dir=True,
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
)

def compute_accuracy(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  return {
      "acc": accuracy_score(p.label_ids, preds),
      "f1": f1_score(p.label_ids, preds),
      "precision": precision_score(p.label_ids, preds),
      "recall": recall_score(p.label_ids, preds)
      }

trainer = AdapterTrainer(
    model=model,
    args=training_args,
    train_dataset=dataset["train"],
    eval_dataset=dataset["validation"],
    compute_metrics=compute_accuracy,
)

# %% [markdown]
# Start the training 🚀

# %%
trainer.train()

# %% [markdown]
# Looks good! Let's evaluate our adapter on the validation split of the dataset to see how well it learned:

# %%
trainer.evaluate()

# %%
print(trainer.predict(dataset['test']))

# %% [markdown]
# We can put our trained model into a `transformers` pipeline to be able to make new predictions conveniently:

# %%
import torch

def predict(sentence1, sentence2):
  encoded = tokenizer(sentence1, sentence2, return_tensors="pt")
  if torch.cuda.is_available():
    encoded.to("cuda")
  logits = model(**encoded)[0]
  pred_class = torch.argmax(logits).item()
  return id2label[pred_class]

#predict(
#    "",
#    ""
#)

# %% [markdown]
# At last, we can also extract the adapter from our model and separately save it for later reuse. Note the size difference compared to a full model!

# %%
model.save_adapter("./mrpc_final_adapter", "mrpc")

# %% [markdown]
# **Share your work!**
# 
# The next step after training is to share our adapter with the world via _AdapterHub_. [Read our guide](https://docs.adapterhub.ml/contributing.html) on how to prepare the adapter module we just saved and contribute it to the Hub!
# 
# ➡️ Also continue with [the next Colab notebook](https://colab.research.google.com/github/Adapter-Hub/adapter-transformers/blob/master/notebooks/02_Adapter_Inference.ipynb) to learn how to use adapters from the Hub.

# %%
